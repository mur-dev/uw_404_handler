# UW 404 Handler Module
This module ensures a 404 response from the server when there is a false URL for custom modules.

---
---
# Functions

---
---
### function function uw_404_handler_init()
Checks to see if the URL the user is requesting is valid. If it is not, it will respond with a 404.